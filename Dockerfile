FROM python:3.6-alpine

RUN mkdir /code
WORKDIR /code

RUN apk add --update --no-cache \
  libsodium-dev \
  gcc \
  musl-dev

ADD requirements.txt /code/
RUN pip install -r requirements.txt

ADD . /code/
